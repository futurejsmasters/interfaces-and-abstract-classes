import Motor from './Motor';

abstract class CarroBase {
  protected motor: Motor;
  public abstract aceite: string;

  constructor(cilindraje: number) {
    this.motor = new Motor(cilindraje);
  }

  public cambiarAceite() {
    this.aceite = this.aceite + 1;
  }

  public abstract arrancarCarro(): void;
  public abstract detenerCarro(): void;
}

export default CarroBase;
