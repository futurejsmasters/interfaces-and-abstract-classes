import Radio from './Radio';
import CarroBase from './CarroBase';

class Mercedes extends CarroBase {
  private _radio: Radio;
  public aceite: string;

  constructor() {
    super(1600);
    this._radio = new Radio();
    this.aceite = 'CASTROL';

    this.cambiarAceite();
  }

  public encenderRadio() {
    if (this._radio.estaEncendido()) {
      console.log('El radio del Mercedes ya esta encendido');
    } else {
      this._radio.encenderRadio();
      console.log('El radio del Mercedes se ha encendido');
    }
  }

  public apagarRadio() {
    if (!this._radio.estaEncendido()) {
      console.log('El radio del Mercedes ya esta apagado');
    } else {
      this._radio.encenderRadio();
      console.log('El radio del Mercedes se ha apagado');
    }
  }

  public arrancarCarro() {
    if (this.motor.estaEncendido()) {
      console.log('El Mercedes ya esta encendido');
    } else {
      this.motor.arrancar();
      this._radio.encenderRadio();
      console.log('El Mercedes se ha encendido');
    }
  }

  public detenerCarro() {
    if (!this.motor.estaEncendido()) {
      console.log('El Mercedes ya esta detenido');
    } else {
      this.motor.apagar();
      console.log('El Mercedes se ha detenido');
    }
  }
}

export default Mercedes;
