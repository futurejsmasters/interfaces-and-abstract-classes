class Motor {
  private _estaEncendido: boolean;
  private _cilindraje: number;

  constructor(cilindraje: number) {
    this._estaEncendido = false;
    this._cilindraje = cilindraje;
  }

  private _encenderValvulas() {
    console.log('Las valvulas se han encendido');
  }

  private _detenerValvulas() {
    console.log('Las valvulas se han apagado');
  }

  public estaEncendido() {
    return this._estaEncendido;
  }

  public arrancar() {
    this._encenderValvulas();
    this._estaEncendido = true;
  }

  public apagar() {
    this._detenerValvulas();
    this._estaEncendido = false;
  }
}

export default Motor;
