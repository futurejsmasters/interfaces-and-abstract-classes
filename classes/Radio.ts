class Radio {
  private _estaEncendido: boolean;

  constructor() {
    this._estaEncendido = false;
  }

  public estaEncendido() {
    return this._estaEncendido;
  }

  public encenderRadio() {
    this._estaEncendido = true;
  }

  public apagarRadio() {
    this._estaEncendido = false;
  }
}

export default Radio;
