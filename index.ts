import Mercedes from './classes/Mercedes';
import CarroBase from './classes/CarroBase';

const carros: CarroBase[] = [new Mercedes()];

for (const carro of carros) {
  carro.arrancarCarro();
}
