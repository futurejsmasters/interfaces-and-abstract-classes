import Motor from '../classes/Motor';

interface Carro {
  motor: Motor;
  arrancarCarro(): void;
  detenerCarro(): void;
}

export default Carro;
